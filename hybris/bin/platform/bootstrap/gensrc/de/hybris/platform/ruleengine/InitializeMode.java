/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:52 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.ruleengine;

public enum InitializeMode
{

	/** <i>Generated enum value</i> for <code>InitializeMode.RESTORE</code> value defined at extension <code>ruleengine</code>. */
	RESTORE , 
	/** <i>Generated enum value</i> for <code>InitializeMode.NEW</code> value defined at extension <code>ruleengine</code>. */
	NEW  

}
