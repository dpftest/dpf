/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:55 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmsfacades.enums;

public enum CMSPageOperation
{

	/** <i>Generated enum value</i> for <code>CMSPageOperation.TRASH_PAGE</code> value defined at extension <code>cmsfacades</code>. */
	TRASH_PAGE  

}
