/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:49 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.solrfacetsearch.config;

public enum QueryMethod
{

	/** <i>Generated enum value</i> for <code>QueryMethod.GET</code> value defined at extension <code>solrfacetsearch</code>. */
	GET , 
	/** <i>Generated enum value</i> for <code>QueryMethod.POST</code> value defined at extension <code>solrfacetsearch</code>. */
	POST  

}
