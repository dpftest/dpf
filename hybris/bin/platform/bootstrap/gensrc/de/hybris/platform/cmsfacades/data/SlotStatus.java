/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:50 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmsfacades.data;

public enum SlotStatus
{

	/** <i>Generated enum value</i> for <code>SlotStatus.TEMPLATE</code> value defined at extension <code>cmsfacades</code>. */
	TEMPLATE , 
	/** <i>Generated enum value</i> for <code>SlotStatus.PAGE</code> value defined at extension <code>cmsfacades</code>. */
	PAGE , 
	/** <i>Generated enum value</i> for <code>SlotStatus.OVERRIDE</code> value defined at extension <code>cmsfacades</code>. */
	OVERRIDE  

}
