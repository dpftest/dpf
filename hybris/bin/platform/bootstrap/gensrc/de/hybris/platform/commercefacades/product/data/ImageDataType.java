/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:56 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commercefacades.product.data;

public enum ImageDataType
{

	/** <i>Generated enum value</i> for <code>ImageDataType.PRIMARY</code> value defined at extension <code>commercefacades</code>. */
	PRIMARY , 
	/** <i>Generated enum value</i> for <code>ImageDataType.GALLERY</code> value defined at extension <code>commercefacades</code>. */
	GALLERY  

}
