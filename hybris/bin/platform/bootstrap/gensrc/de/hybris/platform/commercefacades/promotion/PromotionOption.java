/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:49 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commercefacades.promotion;

public enum PromotionOption
{

	/** <i>Generated enum value</i> for <code>PromotionOption.BASIC</code> value defined at extension <code>commercefacades</code>. */
	BASIC , 
	/** <i>Generated enum value</i> for <code>PromotionOption.EXTENDED</code> value defined at extension <code>commercefacades</code>. */
	EXTENDED  

}
