/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:53 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commercefacades.user;

public enum UserGroupOption
{

	/** <i>Generated enum value</i> for <code>UserGroupOption.BASIC</code> value defined at extension <code>commercefacades</code>. */
	BASIC , 
	/** <i>Generated enum value</i> for <code>UserGroupOption.MEMBERS</code> value defined at extension <code>commercefacades</code>. */
	MEMBERS , 
	/** <i>Generated enum value</i> for <code>UserGroupOption.SUBGROUPS</code> value defined at extension <code>commercefacades</code>. */
	SUBGROUPS  

}
