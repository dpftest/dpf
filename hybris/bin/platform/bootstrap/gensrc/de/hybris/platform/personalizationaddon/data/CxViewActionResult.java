/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:56 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.personalizationaddon.data;

import de.hybris.platform.personalizationservices.data.CxAbstractActionResult;

public  class CxViewActionResult extends CxAbstractActionResult 
{

 

	/** <i>Generated property</i> for <code>CxViewActionResult.type</code> property defined at extension <code>personalizationaddon</code>. */
		
	private String type;
	
	public CxViewActionResult()
	{
		// default constructor
	}
	
	public void setType(final String type)
	{
		this.type = type;
	}

	public String getType() 
	{
		return type;
	}
	


}
