/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:49 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.ycommercewebservices.populator.options;

public enum PaymentInfoOption
{

	/** <i>Generated enum value</i> for <code>PaymentInfoOption.BASIC</code> value defined at extension <code>ycommercewebservices</code>. */
	BASIC , 
	/** <i>Generated enum value</i> for <code>PaymentInfoOption.BILLING_ADDRESS</code> value defined at extension <code>ycommercewebservices</code>. */
	BILLING_ADDRESS  

}
