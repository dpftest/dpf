/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:51 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commercefacades.product.data;

public enum PriceDataType
{

	/** <i>Generated enum value</i> for <code>PriceDataType.BUY</code> value defined at extension <code>commercefacades</code>. */
	BUY , 
	/** <i>Generated enum value</i> for <code>PriceDataType.FROM</code> value defined at extension <code>commercefacades</code>. */
	FROM  

}
