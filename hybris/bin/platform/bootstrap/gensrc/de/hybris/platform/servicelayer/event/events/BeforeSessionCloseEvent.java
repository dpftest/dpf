/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:47 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.servicelayer.event.events;

import java.io.Serializable;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

public  class BeforeSessionCloseEvent  extends AbstractEvent {

	
	public BeforeSessionCloseEvent()
	{
		super();
	}

	public BeforeSessionCloseEvent(final Serializable source)
	{
		super(source);
	}
	


}
