/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:53 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmsfacades.data;

public enum StructureTypeCategory
{

	/** <i>Generated enum value</i> for <code>StructureTypeCategory.COMPONENT</code> value defined at extension <code>cmsfacades</code>. */
	COMPONENT , 
	/** <i>Generated enum value</i> for <code>StructureTypeCategory.PREVIEW</code> value defined at extension <code>cmsfacades</code>. */
	PREVIEW , 
	/** <i>Generated enum value</i> for <code>StructureTypeCategory.PAGE</code> value defined at extension <code>cmsfacades</code>. */
	PAGE , 
	/** <i>Generated enum value</i> for <code>StructureTypeCategory.RESTRICTION</code> value defined at extension <code>cmsfacades</code>. */
	RESTRICTION  

}
