/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:51 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.personalizationyprofile.yaas;

import java.io.Serializable;

public  class ProfileReference  implements Serializable 
{

 	/** Default serialVersionUID value. */
 
 	private static final long serialVersionUID = 1L;

	/** <i>Generated property</i> for <code>ProfileReference.profileId</code> property defined at extension <code>personalizationyprofile</code>. */
		
	private String profileId;
	
	public ProfileReference()
	{
		// default constructor
	}
	
	public void setProfileId(final String profileId)
	{
		this.profileId = profileId;
	}

	public String getProfileId() 
	{
		return profileId;
	}
	


}
