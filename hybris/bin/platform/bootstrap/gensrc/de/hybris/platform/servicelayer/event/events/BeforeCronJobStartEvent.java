/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:51 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.servicelayer.event.events;

import java.io.Serializable;

import de.hybris.platform.servicelayer.event.events.AbstractCronJobPerformEvent;

public  class BeforeCronJobStartEvent extends AbstractCronJobPerformEvent {

	
	public BeforeCronJobStartEvent()
	{
		super();
	}

	public BeforeCronJobStartEvent(final Serializable source)
	{
		super(source);
	}
	


}
