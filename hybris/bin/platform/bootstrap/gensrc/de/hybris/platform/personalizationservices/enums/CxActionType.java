/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:55 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.personalizationservices.enums;

public enum CxActionType
{

	/** <i>Generated enum value</i> for <code>CxActionType.CxAbstractAction</code> value defined at extension <code>personalizationservices</code>. */
	CXABSTRACTACTION , 
	/** <i>Generated enum value</i> for <code>CxActionType.CxCmsAction</code> value defined at extension <code>personalizationcms</code>. */
	CXCMSACTION  

}
