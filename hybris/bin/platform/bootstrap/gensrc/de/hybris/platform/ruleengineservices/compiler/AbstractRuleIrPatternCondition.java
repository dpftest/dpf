/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:47 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.ruleengineservices.compiler;

import de.hybris.platform.ruleengineservices.compiler.RuleIrCondition;

public abstract  class AbstractRuleIrPatternCondition extends RuleIrCondition 
{

 

	/** <i>Generated property</i> for <code>AbstractRuleIrPatternCondition.variable</code> property defined at extension <code>ruleengineservices</code>. */
		
	private String variable;
	
	public AbstractRuleIrPatternCondition()
	{
		// default constructor
	}
	
	public void setVariable(final String variable)
	{
		this.variable = variable;
	}

	public String getVariable() 
	{
		return variable;
	}
	


}
