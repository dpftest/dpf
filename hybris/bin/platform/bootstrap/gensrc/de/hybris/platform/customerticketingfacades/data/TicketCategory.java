/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:49 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.customerticketingfacades.data;

public enum TicketCategory
{

	/** <i>Generated enum value</i> for <code>TicketCategory.Enquiry</code> value defined at extension <code>customerticketingfacades</code>. */
	ENQUIRY , 
	/** <i>Generated enum value</i> for <code>TicketCategory.Complaint</code> value defined at extension <code>customerticketingfacades</code>. */
	COMPLAINT , 
	/** <i>Generated enum value</i> for <code>TicketCategory.Problem</code> value defined at extension <code>customerticketingfacades</code>. */
	PROBLEM  

}
