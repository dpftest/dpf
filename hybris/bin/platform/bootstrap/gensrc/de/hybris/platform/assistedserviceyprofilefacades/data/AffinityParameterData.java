/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:46 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.assistedserviceyprofilefacades.data;

import java.io.Serializable;

public  class AffinityParameterData  implements Serializable 
{

 	/** Default serialVersionUID value. */
 
 	private static final long serialVersionUID = 1L;

	/** <i>Generated property</i> for <code>AffinityParameterData.sizeLimit</code> property defined at extension <code>assistedserviceyprofilefacades</code>. */
		
	private int sizeLimit;
	
	public AffinityParameterData()
	{
		// default constructor
	}
	
	public void setSizeLimit(final int sizeLimit)
	{
		this.sizeLimit = sizeLimit;
	}

	public int getSizeLimit() 
	{
		return sizeLimit;
	}
	


}
