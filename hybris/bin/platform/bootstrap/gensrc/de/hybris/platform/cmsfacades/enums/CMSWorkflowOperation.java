/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:46 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmsfacades.enums;

public enum CMSWorkflowOperation
{

	/** <i>Generated enum value</i> for <code>CMSWorkflowOperation.CANCEL_WORKFLOW</code> value defined at extension <code>cmsfacades</code>. */
	CANCEL_WORKFLOW , 
	/** <i>Generated enum value</i> for <code>CMSWorkflowOperation.MAKE_DECISION</code> value defined at extension <code>cmsfacades</code>. */
	MAKE_DECISION  

}
