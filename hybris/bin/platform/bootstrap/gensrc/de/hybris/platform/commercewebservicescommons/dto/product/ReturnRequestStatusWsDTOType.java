/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:47 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commercewebservicescommons.dto.product;

public enum ReturnRequestStatusWsDTOType
{

	/** <i>Generated enum value</i> for <code>ReturnRequestStatusWsDTOType.CANCELLING</code> value defined at extension <code>commercewebservicescommons</code>. */
	CANCELLING  

}
