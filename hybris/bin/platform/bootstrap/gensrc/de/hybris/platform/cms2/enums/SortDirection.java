/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:46 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cms2.enums;

public enum SortDirection
{

	/** <i>Generated enum value</i> for <code>SortDirection.ASC</code> value defined at extension <code>cms2</code>. */
	ASC , 
	/** <i>Generated enum value</i> for <code>SortDirection.DESC</code> value defined at extension <code>cms2</code>. */
	DESC  

}
