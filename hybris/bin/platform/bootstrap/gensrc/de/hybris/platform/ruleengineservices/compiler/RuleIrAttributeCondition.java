/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:54 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.ruleengineservices.compiler;

import de.hybris.platform.ruleengineservices.compiler.AbstractRuleIrAttributeCondition;

public  class RuleIrAttributeCondition extends AbstractRuleIrAttributeCondition 
{

 

	/** <i>Generated property</i> for <code>RuleIrAttributeCondition.value</code> property defined at extension <code>ruleengineservices</code>. */
		
	private Object value;
	
	public RuleIrAttributeCondition()
	{
		// default constructor
	}
	
	public void setValue(final Object value)
	{
		this.value = value;
	}

	public Object getValue() 
	{
		return value;
	}
	


}
