/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:49 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.warehouse;

public enum WarehouseConsignmentStatus
{

	/** <i>Generated enum value</i> for <code>WarehouseConsignmentStatus.CANCEL</code> value defined at extension <code>basecommerce</code>. */
	CANCEL , 
	/** <i>Generated enum value</i> for <code>WarehouseConsignmentStatus.PARTIAL</code> value defined at extension <code>basecommerce</code>. */
	PARTIAL , 
	/** <i>Generated enum value</i> for <code>WarehouseConsignmentStatus.COMPLETE</code> value defined at extension <code>basecommerce</code>. */
	COMPLETE  

}
