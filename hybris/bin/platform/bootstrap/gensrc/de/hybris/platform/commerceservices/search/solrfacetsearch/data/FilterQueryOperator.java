/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:45 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commerceservices.search.solrfacetsearch.data;

public enum FilterQueryOperator
{

	/** <i>Generated enum value</i> for <code>FilterQueryOperator.AND</code> value defined at extension <code>commerceservices</code>. */
	AND , 
	/** <i>Generated enum value</i> for <code>FilterQueryOperator.OR</code> value defined at extension <code>commerceservices</code>. */
	OR  

}
