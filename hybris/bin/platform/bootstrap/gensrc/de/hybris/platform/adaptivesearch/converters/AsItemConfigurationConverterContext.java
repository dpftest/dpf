/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:52 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.adaptivesearch.converters;

import de.hybris.platform.adaptivesearch.converters.AsConfigurationConverterContext;

public  class AsItemConfigurationConverterContext extends AsConfigurationConverterContext 
{

 

	/** <i>Generated property</i> for <code>AsItemConfigurationConverterContext.searchProfileCode</code> property defined at extension <code>adaptivesearch</code>. */
		
	private String searchProfileCode;

	/** <i>Generated property</i> for <code>AsItemConfigurationConverterContext.searchConfigurationUid</code> property defined at extension <code>adaptivesearch</code>. */
		
	private String searchConfigurationUid;
	
	public AsItemConfigurationConverterContext()
	{
		// default constructor
	}
	
	public void setSearchProfileCode(final String searchProfileCode)
	{
		this.searchProfileCode = searchProfileCode;
	}

	public String getSearchProfileCode() 
	{
		return searchProfileCode;
	}
	
	public void setSearchConfigurationUid(final String searchConfigurationUid)
	{
		this.searchConfigurationUid = searchConfigurationUid;
	}

	public String getSearchConfigurationUid() 
	{
		return searchConfigurationUid;
	}
	


}
