/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:49 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.solrfacetsearch.config;

public enum CommitMode
{

	/** <i>Generated enum value</i> for <code>CommitMode.NEVER</code> value defined at extension <code>solrfacetsearch</code>. */
	NEVER , 
	/** <i>Generated enum value</i> for <code>CommitMode.AFTER_INDEX</code> value defined at extension <code>solrfacetsearch</code>. */
	AFTER_INDEX , 
	/** <i>Generated enum value</i> for <code>CommitMode.AFTER_BATCH</code> value defined at extension <code>solrfacetsearch</code>. */
	AFTER_BATCH , 
	/** <i>Generated enum value</i> for <code>CommitMode.MIXED</code> value defined at extension <code>solrfacetsearch</code>. */
	MIXED  

}
