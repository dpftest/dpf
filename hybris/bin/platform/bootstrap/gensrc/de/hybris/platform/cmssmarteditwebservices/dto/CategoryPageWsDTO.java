/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:48 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmssmarteditwebservices.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @deprecated since 1811, no longer needed.
 */
@ApiModel(value="CategoryPageWsDTO")
@Deprecated(forRemoval = true)
public  class CategoryPageWsDTO extends AbstractPageWsDTO 
{

 
	
	public CategoryPageWsDTO()
	{
		// default constructor
	}
	


}
