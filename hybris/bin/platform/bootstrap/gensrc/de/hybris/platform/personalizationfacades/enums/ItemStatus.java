/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:49 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.personalizationfacades.enums;

public enum ItemStatus
{

	/** <i>Generated enum value</i> for <code>ItemStatus.ENABLED</code> value defined at extension <code>personalizationfacades</code>. */
	ENABLED , 
	/** <i>Generated enum value</i> for <code>ItemStatus.DISABLED</code> value defined at extension <code>personalizationfacades</code>. */
	DISABLED , 
	/** <i>Generated enum value</i> for <code>ItemStatus.DELETED</code> value defined at extension <code>personalizationfacades</code>. */
	DELETED  

}
