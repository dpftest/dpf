/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:45 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.acceleratorstorefrontcommons.consent.data;

import de.hybris.platform.commercefacades.consent.data.AnonymousConsentData;

/**
 * @deprecated Since 1905. Use {@link AnonymousConsentData} instead
 */
@Deprecated(forRemoval = true)
public  class ConsentCookieData extends AnonymousConsentData 
{

 
	
	public ConsentCookieData()
	{
		// default constructor
	}
	


}
