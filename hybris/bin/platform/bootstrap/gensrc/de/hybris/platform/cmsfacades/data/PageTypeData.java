/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 26, 2020, 11:33:48 AM
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmsfacades.data;

import de.hybris.platform.cmsfacades.data.ComposedTypeData;

public  class PageTypeData extends ComposedTypeData 
{

 

	/** <i>Generated property</i> for <code>PageTypeData.type</code> property defined at extension <code>cmsfacades</code>. */
		
	private String type;
	
	public PageTypeData()
	{
		// default constructor
	}
	
	/**
	 * @deprecated since 1811
	 */
	@Deprecated(forRemoval = true)
	public void setType(final String type)
	{
		this.type = type;
	}

	/**
	 * @deprecated since 1811
	 */
	@Deprecated(forRemoval = true)
	public String getType() 
	{
		return type;
	}
	


}
