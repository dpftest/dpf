/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 26, 2020, 11:33:24 AM                   ---
 * ----------------------------------------------------------------
 */
package com.hybris.merchandising.storefrontsampledata.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedMerchandisingstorefrontsampledataaddonConstants
{
	public static final String EXTENSIONNAME = "merchandisingstorefrontsampledataaddon";
	
	protected GeneratedMerchandisingstorefrontsampledataaddonConstants()
	{
		// private constructor
	}
	
	
}
