/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 26, 2020, 11:33:24 AM                   ---
 * ----------------------------------------------------------------
 */
package com.hybris.yprofile.profiletagaddon.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedProfiletagaddonConstants
{
	public static final String EXTENSIONNAME = "profiletagaddon";
	public static class TC
	{
		public static final String PROFILETAGSCRIPTCOMPONENT = "ProfileTagScriptComponent".intern();
	}
	
	protected GeneratedProfiletagaddonConstants()
	{
		// private constructor
	}
	
	
}
